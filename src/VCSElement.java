import com.google.java.contract.Ensures;
import com.google.java.contract.Invariant;
import com.google.java.contract.Requires;

@Invariant({
	"true"
})
public class VCSElement {

	public enum ElementState {
		CHECKEDIN,
		CHECKEDOUT
	}
	
	public VCSElement(String name, String content) {
		
		this.state = ElementState.CHECKEDIN;
		this.name = name;
		this.oldContent = this.content = content;
		version = 1;
	}
	
	
	public ElementState getState() {
		return state;
	}
	

	public void changeState(VCS.VCSEvent event) throws VCSException {
		
		setState(event);
	}

	public String getName() {
		return name;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String ct) {
		this.content = ct;
	}
	
	public boolean hasChanged() {
		return !content.equals(oldContent);
	}
	
	public int getVersion() {
		return version;
	}
	
	
	private void setState(VCS.VCSEvent event) {
		
		if(event == VCS.VCSEvent.UNDOCHECKOUT) {
			state = ElementState.CHECKEDIN;
			content = oldContent;
		} else if (event == VCS.VCSEvent.CHECKOUT) {
			state = ElementState.CHECKEDOUT;
			oldContent = content;
		} else {
			state = ElementState.CHECKEDIN;
			oldContent = content;
			version++;
		}
	}
	

	private ElementState state;
	private String name;
	private String content;
	private String oldContent;
	private int version;
	
}
